# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("comments")
class CommentRecord:
  """ Stores information about comments on objects.
  """

  schema = {

    # Primary Key
    "id": {
      "type": "integer",
      "primary": True
    },

    # Object to which the comment belongs
    "internal_object_id": {
      "foreign" : {
        "table": "objects",
        "key":   "id"
      }
    },

    # The global identifier for this comment
    "uid": {
      "type": "string",
      "unique": True,
      "length": 128
    },

    # The revision of the object as it was seen when commented
    "object_revision": {
      "type": "string",
      "length": 128
    },

    # The Occam UUID for the person that commented
    "commenter_uid": {
      "type": "string",
      "length": 36
    },

    # The name of the commenter
    "commenter_name": {
      "type": "string",
      "length": 128
    },

    # The comment content
    "content": {
      "type": "text"
    },

    # The reply to tag
    "in_reply_to": {
      "foreign" : {
        "table": "comments",
        "key":   "uid"
      }
    },
    
    # The Occam node that is the source of this comment
    "origin": {
      "type": "string",
      "length": 128
    },

    # The identifiable alias to use when "anonymous"
    "anonymous_alias": {
      "type": "string",
      "length": 80
    },

    # The time the comment was posted
    "create_time": {
      "type": "datetime"
    },

    # The time the comment was updated
    "update_time": {
      "type": "datetime"
    }
  }
